const sentHttpRequest = (method, url, token) => {
    return fetch(url, {
        method: method,
        headers: { 'Content-Type': 'application/json', 'Authorization': token }
    }).then(response => {
        return response.json();
    });
};

function getTimetable(from, to) {
    // var token = window.localStorage.getItem("token");
    // var userId = window.localStorage.getItem("user-id");

    var token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTcwOTI1NTQyLCJleHAiOjE1NzE1MzAzNDIsInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX1VTRVIifV19.mc8fJlE1FpAbKipLdf6QT4I8fao1ALpMF0zWoT05BqXxJSfo3e_2rY0N2ZElbTkj-Gxge_4RfAiuw7DqN9K4BQ";
    var userId = 1;

    sentHttpRequest(
        'GET',
        'http://localhost:9999/api/test/user/timetable?from=' + from + '&to=' + to + '&userId=' + userId,
        'Bearer ' + token
    ).then(response => {
        var head = document.getElementById("head-timetable");
        head.innerHTML = ''
        for (var i in response.date) {
            if (i == 0) {
                var tagColumn = document.createElement("th");
                var textNode = document.createTextNode("#");
                tagColumn.appendChild(textNode);
                head.appendChild(tagColumn)
            }
            var tagColumn = document.createElement("th");
            var textNode = document.createTextNode(response.date[i]);
            tagColumn.appendChild(textNode);
            head.appendChild(tagColumn)
        }

        var body = document.getElementById("body-timetable");
        body.innerHTML = ''
        for(var i of response.data){
            var tagRow = document.createElement("tr");
            body.appendChild(tagRow);
            for(date in i.date_detail){
                if(date == 0){
                    var tagColumn = document.createElement("th");
                    tagColumn.width = "14%"
                    var textNode = document.createTextNode(i.subject_title)
                    tagColumn.appendChild(textNode)
                    tagRow.appendChild(tagColumn)   
                }
                var tagColumn = document.createElement("th");
                tagColumn.width = "10%"
                var textNode = document.createTextNode(i.date_detail[date].timetable)
                tagColumn.appendChild(textNode)
                tagRow.appendChild(tagColumn)
            }
        }
    }).catch(err => {
        alert(err);
    })
}

function formatDate(dateObj){
    return dateObj.getFullYear() + "-" + (dateObj.getMonth() + 1) + "-" + dateObj.getDate();
}

function addDays(dateObj, numDays) {
    dateObj.setDate(dateObj.getDate() + numDays);
    return formatDate(dateObj);
}

function mulDays(dateObj, numDays) {
    dateObj.setDate(dateObj.getDate() - numDays);
    return formatDate(dateObj);
}

function createDateDisplay(dateObj){
    var dateArray = dateObj.split('-');
    return dateArray[1] + "/" + dateArray[2] +  "/" + dateArray[0];
}

function getOtherCoursesetDate(){
    var inputValue = document.getElementById("distanceDate");
    var curentDate = new Date();
    var from = ''
    var to = ''
    switch (curentDate.getDay()) {
        case 0:
            from = addDays(curentDate, 1);
            to = addDays(curentDate, 6);
            break;
        case 1:
            from = addDays(curentDate, 0);
            to = addDays(curentDate, 5);
            break;
        case 2:
            from = mulDays(curentDate, 1);
            to = addDays(curentDate, 4);
            break;
        case 3:
            from = mulDays(curentDate, 2);
            to = addDays(curentDate, 3);
            break;
        case 4:
            from = mulDays(curentDate, 3);
            to = addDays(curentDate, 2);
            break;
        case 5:
            from = mulDays(curentDate, 4);
            to = addDays(curentDate, 1);
            break;
        case 6:
            from = mulDays(curentDate, 5);
            to = addDays(curentDate, 0);
    }
    inputValue.value = createDateDisplay(from) + " - " + createDateDisplay(to);
    getTimetable(from, to);
}

document.getElementById("timetable-title").addEventListener("load", setDate());
