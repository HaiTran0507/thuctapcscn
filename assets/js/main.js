$.noConflict();

jQuery(document).ready(function($) {
  "use strict";

  [].slice
    .call(document.querySelectorAll("select.cs-select"))
    .forEach(function(el) {
      new SelectFx(el);
    });

  jQuery(".selectpicker").selectpicker;

  $(".search-trigger").on("click", function(event) {
    event.preventDefault();
    event.stopPropagation();
    $(".search-trigger")
      .parent(".header-left")
      .addClass("open");
  });

  $(".search-close").on("click", function(event) {
    event.preventDefault();
    event.stopPropagation();
    $(".search-trigger")
      .parent(".header-left")
      .removeClass("open");
  });

  $(".equal-height").matchHeight({
    property: "max-height"
  });

  // var chartsheight = $('.flotRealtime2').height();
  // $('.traffic-chart').css('height', chartsheight-122);

  // Counter Number
  $(".count").each(function() {
    $(this)
      .prop("Counter", 0)
      .animate(
        {
          Counter: $(this).text()
        },
        {
          duration: 3000,
          easing: "swing",
          step: function(now) {
            $(this).text(Math.ceil(now));
          }
        }
      );
  });

  // Menu Trigger
  $("#menuToggle").on("click", function(event) {
    var windowWidth = $(window).width();
    if (windowWidth < 1010) {
      $("body").removeClass("open");
      if (windowWidth < 760) {
        $("#left-panel").slideToggle();
      } else {
        $("#left-panel").toggleClass("open-menu");
      }
    } else {
      $("body").toggleClass("open");
      $("#left-panel").removeClass("open-menu");
    }
  });

  $(".menu-item-has-children.dropdown").each(function() {
    $(this).on("click", function() {
      var $temp_text = $(this)
        .children(".dropdown-toggle")
        .html();
      $(this)
        .children(".sub-menu")
        .prepend('<li class="subtitle">' + $temp_text + "</li>");
    });
  });

  // Load Resize
  $(window).on("load resize", function(event) {
    var windowWidth = $(window).width();
    if (windowWidth < 1010) {
      $("body").addClass("small-device");
    } else {
      $("body").removeClass("small-device");
    }
  });

  $(window).on("load", function() {
    let logout = $("#logout");
    logout.click(function() {
      let access_token = window.localStorage.getItem("access_token");
      let role = window.localStorage.getItem("ROLE");
      localStorage.removeItem("access_token");
      localStorage.removeItem("ROLE");
      localStorage.removeItem("userId");
      window.location.assign("/");
    });

    // var start_time = new TimePicker("start_time", {
    //   lang: "en",
    //   theme: "dark"
    // });
    // var end_time = new TimePicker("end_time", {
    //   lang: "en",
    //   theme: "dark"
    // });
    // start_time.on("change", function(evt) {
    //   var value = (evt.hours || "00") + ":" + (evt.minute || "00");
    //   evt.element.value = value;
    // });
    // end_time.on("change", function(evt) {
    //   var value = (evt.hours || "00") + ":" + (evt.minute || "00");
    //   evt.element.value = value;
    // });
  });
});
