const sentHttpRequest = (method, url, token) => {
    return fetch(url, {
        method: method,
        headers: { 'Content-Type': 'application/json', 'Authorization': token }
    }).then(response => {
        return response.json();
    });
};

function updateOtherCourse(courses){

    for(var element in courses){
        console.log(element)
    }

    var body = document.getElementById("couse-other");

        var divElement = document.createElement("div")
        divElement.className = "col-md-4 custom-display-course"

        body.appendChild(divElement)

        var asideElement = document.createElement("aside")
        asideElement.className = "profile-nav alt";
        divElement.appendChild(asideElement)

        var sectionElement = document.createElement("section")
        sectionElement.className = "card"
        asideElement.appendChild(sectionElement)

        var divCard = document.createElement("div")
        divCard.className = "card-header user-header alt custom-color-bg"
        sectionElement.appendChild(divCard)

        var divMedia = document.createElement("div")
        divMedia.className = "media"
        divCard.appendChild(divMedia)

        var divMediaA = document.createElement("a")
        divMedia.appendChild(divMediaA)


        var img = document.createElement("img")
        img.className = "align-self-center rounded-circle mr-3"
        img.style = "width:85px; height:85px;"
        img.src = "images/admin.jpg"

        divMediaA.appendChild(img)

        var divMediaBody = document.createElement("div")
        divMediaBody.className = "media-body"

        divMedia.appendChild(divMediaBody)

        var h3MediaBody = document.createElement("h3")
        divMediaBody.appendChild(h3MediaBody)

        var textName = document.createTextNode("Jim Doe")
        h3MediaBody.appendChild(textName)

        var pMediaBody = document.createElement("p")
        divMediaBody.appendChild(pMediaBody)

        var textTech = document.createTextNode("Project Manager")
        pMediaBody.appendChild(textTech)

        var h4MediaBody = document.createElement("h4")
        h4MediaBody.classList = "text-light"
        h4MediaBody.style = "text-align: center"
        divMediaBody.appendChild(h4MediaBody)

        var nameCourse = document.createTextNode("Tiêng anh chuyên ngành")
        h4MediaBody.appendChild(nameCourse)

        var ulListGroup = document.createElement("ul")
        ulListGroup.className = "list-group list-group-flush"
        sectionElement.appendChild(ulListGroup)


        var liListGroup = document.createElement("li")
        liListGroup.className = "list-group-item"
        ulListGroup.appendChild(liListGroup)

        var aListGroup = document.createElement("a")
        aListGroup.href = "#"
        liListGroup.appendChild(aListGroup)

        var iListGroup = document.createElement("i")
        iListGroup.className = "fa fa-envelope-o"
        aListGroup.appendChild(iListGroup)

        var textTech = document.createTextNode("Số lượng học viên")
        aListGroup.appendChild(textTech)

        var spanListGroup = document.createElement("span")
        spanListGroup.className = "badge badge-primary pull-right"
        aListGroup.appendChild(spanListGroup)

        var textPercent = document.createTextNode("15/30")
        spanListGroup.appendChild(textPercent)

        //element-1
        var liListGroup1 = document.createElement("li")
        liListGroup1.className = "list-group-item"
        ulListGroup.appendChild(liListGroup1)

        var aListGroup = document.createElement("a")
        aListGroup.href = "#"
        liListGroup1.appendChild(aListGroup)

        var iListGroup = document.createElement("i")
        iListGroup.className = "fa fa-tasks"
        aListGroup.appendChild(iListGroup)

        var textTech = document.createTextNode("Nội dung khoá học")
        aListGroup.appendChild(textTech)

        var spanListGroup = document.createElement("span")
        spanListGroup.className = "badge badge-danger pull-right"
        aListGroup.appendChild(spanListGroup)

        var textPercent = document.createTextNode("View")
        spanListGroup.appendChild(textPercent)

        //element-2
        var liListGroup2 = document.createElement("li")
        liListGroup2.className = "list-group-item"
        ulListGroup.appendChild(liListGroup2)

        var aListGroup = document.createElement("a")
        aListGroup.href = "#"
        liListGroup2.appendChild(aListGroup)

        var iListGroup = document.createElement("i")
        iListGroup.className = "fa fa-bell-o"
        aListGroup.appendChild(iListGroup)

        var textTech = document.createTextNode("Thông tin giảng viên")
        aListGroup.appendChild(textTech)

        var spanListGroup = document.createElement("span")
        spanListGroup.className = "badge badge-success pull-right"
        aListGroup.appendChild(spanListGroup)

        var textPercent = document.createTextNode("View")
        spanListGroup.appendChild(textPercent)

        //element-3
        var liListGroup3 = document.createElement("li")
        liListGroup3.className = "list-group-item"
        ulListGroup.appendChild(liListGroup3)

        var aListGroup = document.createElement("a")
        liListGroup3.appendChild(aListGroup)

        var iListGroup = document.createElement("i")
        iListGroup.className = "fa fa-comments-o"
        aListGroup.appendChild(iListGroup)

        var textTech = document.createTextNode("Thời hạn đăng ký")
        aListGroup.appendChild(textTech)

        var spanListGroup = document.createElement("span")
        spanListGroup.className = "badge badge-warning pull-right r-activity"
        aListGroup.appendChild(spanListGroup)

        var textPercent = document.createTextNode("20/11/2019")
        spanListGroup.appendChild(textPercent)

        //element-4
        var liListGroup4 = document.createElement("li")
        liListGroup4.className = "list-group-item"
        ulListGroup.appendChild(liListGroup4)

        var buttonRegister = document.createElement("button")
        buttonRegister.type = "button"
        buttonRegister.className = "btn btn-outline-secondary btn-lg btn-block custom-button"
        liListGroup4.appendChild(buttonRegister)

        var textTech = document.createTextNode("Đăng ký")
        buttonRegister.appendChild(textTech)
}

function getOtherCourse() {

    var token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTcxNzQ2ODQ5LCJleHAiOjE1NzIzNTE2NDksInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dfQ.w9Jr1EMr5nHnuPh9zzyo2x8M-x-Df9Pr5Bnj2uC-qX_0-0RuyT61nuHCUhWxCbmP55DoBjUQksD5rhSS74kE-Q";
    var userId = 10004;

    sentHttpRequest(
        'GET',
        'http://localhost:9999/api/course/another-couse?userId='+ userId,
        'Bearer ' + token
    ).then(response => {
        updateOtherCourse(response)
    }).catch(err => {
        console.log(err)
    })
}

document.getElementById("couse-other").addEventListener("load", getOtherCourse())
