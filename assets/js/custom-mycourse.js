const sentHttpRequest = (method, url, token) => {
    return fetch(url, {
        method: method,
        headers: { 'Content-Type': 'application/json', 'Authorization': token }
    }).then(response => {
        return response.json();
    });
};

function getCourseStudent() {
    // var token = window.localStorage.getItem("token");
    // var userId = window.localStorage.getItem("user-id");

    var token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTcxNTU0MzgwLCJleHAiOjE1NzIxNTkxODAsInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dfQ.y5Cy_gEeWU8U03RcSSgHWF8v5jcIHWQ-Rqj_Jsx1M1HtQ65QIi1VWxq5SLvSs-01dnQoL9bGa7eHrorZEuFAsw";
    var userId = 1;

    sentHttpRequest(
        'GET',
        'http://localhost:9999/api/course/my-course?userId=' + userId,
        'Bearer ' + token
    ).then(response => {
        var bodyCouse = document.getElementById("body-course");

        for (var course of response) {
            var tagRow = document.createElement("tr")
            bodyCouse.appendChild(tagRow);

            for (field in course) {
                var tagColumn = document.createElement("td");
                var textNode = document.createTextNode(course[field]);
                tagColumn.appendChild(textNode);
                tagRow.appendChild(tagColumn);
            }
        }
    }).catch(err => {
        alert(err);
    })
}

document.getElementById("body-course").addEventListener("load", getCourseStudent());

