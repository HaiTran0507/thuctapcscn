Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#teacherIndex",
  data() {
    return {
      access_token: "",
      userId: "",
      list_cours: [],
      checkin: {
        courseId: "",
        teacherId: "",
        studentId: []
      },
      students: []
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    this.userId = window.localStorage.getItem("userId");
    if (
      role != "ROLE_TEACHER" ||
      !role ||
      !access_token ||
      access_token == ""
    ) {
      window.location.assign("/");
    }
    this.getListCours();
  },
  methods: {
    getListCours() {
      this.$http
        .get(url.host + url.link_course + `/my-course?userId= ${this.userId}`, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_cours = response.data;
              // console.log(this.list_cours);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    showTable(item) {
      var tableCheckin = document.getElementById("tableCheckin");
      this.tableCheckin = "Sửa tài khoản";
      this.getListStudent(item.id);
      this.checkin.courseId = item.id;
      this.checkin.teacherId = this.userId;
      bootbox
        .dialog({
          message: tableCheckin,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: true,
          className: "fullscreen",
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          tableCheckin.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          tableCheckin.style.display = "none";
          document.body.appendChild(tableCheckin);
        })
        .modal("show");
    },
    getListStudent(tuitionId) {
      this.$http
        .get(url.host + url.link_course + "/tuition/" + tuitionId, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.students = response.data;
              console.log(this.students);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },

    checkInSt() {
      var dt = new Date(),
        dformat = `${dt
          .getFullYear()
          .toString()
          .padStart(4, "0")}-${(dt.getMonth() + 1)
          .toString()
          .padStart(2, "0")}-${dt
          .getDate()
          .toString()
          .padStart(2, "0")} ${dt
          .getHours()
          .toString()
          .padStart(2, "0")}:${dt
          .getMinutes()
          .toString()
          .padStart(2, "0")}:${dt
          .getSeconds()
          .toString()
          .padStart(2, "0")}`;
      this.$http
        .post(
          url.host + url.link_checkin,
          {
            courseId: this.checkin.courseId,
            teacherId: this.checkin.teacherId,
            students: this.checkin.studentId
          },
          {
            headers: {
              Authorization: this.access_token
            }
          }
        )
        .then(
          function(response) {
            if (response.status == 200) {
              // this.getListAccount();
              toastr.info("Điểm danh thành công", "Thành công!", {
                timeOut: 2000
              });
            } else {
              toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                timeOut: 2000
              });
            }
          },
          function(response) {
            toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
              timeOut: 2000
            });
            console.error(response);
          }
        );
      this.$http
        .post(
          url.host + url.link_timekeeping,
          {
            userId: this.userId,
            timeCheckin: dformat
          },
          {
            headers: {
              Authorization: this.access_token
            }
          }
        )
        .then(
          function(response) {
            if (response.status == 200) {
              // toastr.info("Điểm danh thành công", "Thành công!", {
              //   timeOut: 2000
              // });
            } else {
              // toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
              //   timeOut: 2000
              // });
            }
          },
          function(response) {
            toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
              timeOut: 2000
            });
            console.error(response);
          }
        );
    },
    filters: {
      date(value) {
        var date = new Date(value);
        return (
          date.getDate() +
          "/" +
          (date.getMonth() + 1) +
          "/" +
          date.getFullYear()
        );
      }
    }
  }
});
