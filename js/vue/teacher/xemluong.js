Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#teacherXemLuong",
  data() {
    return {
      access_token: "",
      userId: "",
      start_time: "",
      end_time: "",
      total: 0,
      money: 0,
      list_time: []
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    this.userId = window.localStorage.getItem("userId");
    if (
      role != "ROLE_TEACHER" ||
      !role ||
      !access_token ||
      access_token == ""
    ) {
      window.location.assign("/");
    }
    // this.getListCours();
  },
  methods: {
    Caculate() {
      if (this.start_time != "" && this.end_time != "") {
        let start = this.formatDate(this.start_time);
        let end = this.formatDate(this.end_time);
        this.$http
          .get(
            url.host +
              url.link_caculate +
              `?id=${this.userId}&start=${start}&end=${end}`,
            {
              headers: {
                Authorization: this.access_token
              }
            }
          )
          .then(
            function(response) {
              if (response.status == 200) {
                console.log(response.data.data);
                this.money = response.data.total_money;
                this.total = response.data.total;
                this.list_time = response.data.data;
              }
            },
            function(response) {
              console.error(response);
            }
          );
      }
    },

    formatDate(value) {
      var dt = new Date(value),
        dformat = `${dt
          .getFullYear()
          .toString()
          .padStart(4, "0")}-${(dt.getMonth() + 1)
          .toString()
          .padStart(2, "0")}-${dt
          .getDate()
          .toString()
          .padStart(2, "0")}`;
      return dformat;
    },
    filters: {
      date(value) {
        var date = new Date(value);
        return (
          date.getDate() +
          "/" +
          (date.getMonth() + 1) +
          "/" +
          date.getFullYear()
        );
      }
    }
  },
  filters: {
    date(value) {
      var dt = new Date(value),
        dformat = `${dt
          .getFullYear()
          .toString()
          .padStart(4, "0")}-${(dt.getMonth() + 1)
          .toString()
          .padStart(2, "0")}-${dt
          .getDate()
          .toString()
          .padStart(2, "0")} (${dt
          .getHours()
          .toString()
          .padStart(2, "0")}:${dt
          .getMinutes()
          .toString()
          .padStart(2, "0")}:${dt
          .getSeconds()
          .toString()
          .padStart(2, "0")})`;
      return dformat;
    },
    money(value) {
      var x = value;
      x = x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
      return x + " VNĐ";
    }
  }
});
