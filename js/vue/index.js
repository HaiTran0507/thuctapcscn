import * as path from "../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#app",
  data() {
    return {
      account: {
        username: "",
        password: ""
      },
      popup: false,
      access_token: "",
      role: "",
      list_adv: [],
      demand: {
        fullName: "",
        phone: "",
        needs: "",
        time: ""
      }
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    var role = window.localStorage.getItem("ROLE");
    if (role == "ROLE_ADMIN" && access_token != "") {
      window.location.assign("page/admin/admin.html");
    } else if (role == "ROLE_USER" && access_token != "") {
      window.location.assign("page/student/student.html");
    } else if (role == "ROLE_TEACHER" && access_token != "") {
      window.location.assign("page/teacher/teacher.html");
    }
    this.getAllAdv();
  },
  methods: {
    handleSubmitLogin() {
      if (this.account.username != "" && this.account.password != "") {
        this.$http
          .post(
            url.host + url.signin,
            {
              username: this.account.username,
              password: this.account.password
            },
            {}
          )
          .then(
            function(response) {
              if (response.status == 200) {
                var decoded = jwt_decode(response.data.accessToken);
                var data = response.data;
                console.log(response);
                delete data.statusCode;
                console.log(decoded);
                window.localStorage.setItem(
                  "access_token",
                  data.tokenType + " " + data.accessToken
                );
                window.localStorage.setItem("userId", decoded.sub);
                // window.location.replace('admin.html');assign
                window.localStorage.setItem("ROLE", decoded.roles[0].authority);
                if (decoded.roles[0].authority == "ROLE_ADMIN") {
                  window.location.assign("page/admin/admin.html");
                } else if (decoded.roles[0].authority == "ROLE_USER") {
                  window.location.assign("/page/student/student.html");
                } else if (decoded.roles[0].authority == "ROLE_TEACHER") {
                  window.location.assign("page/teacher/teacher.html");
                }
              }
            },
            function(response) {
              //tai khoan chua co
              // console.error(response)
              toastr.error(
                "Tài khoản chưa có hoặc bạn đăng nhập chưa đúng",
                "Gặp lỗi!",
                { timeOut: 2000 }
              );
            }
          );
      } else {
        toastr.error(
          "Tài khoản chưa có hoặc bạn đăng nhập chưa đúng",
          "Gặp lỗi!",
          { timeOut: 2000 }
        );
      }
    },
    showPopup(value) {
      // this.popup = value;
      var modal_content = document.getElementById("modal-content");
      bootbox
        .dialog({
          message: modal_content,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: true,
          className: "fullscreen",
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          modal_content.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          modal_content.style.display = "none";
          document.body.appendChild(modal_content);
        })
        .modal("show");
    },
    getAllAdv() {
      this.$http.get(url.host + url.get_adv).then(
        function(response) {
          if (response.status == 200) {
            console.log(response);
            this.list_adv = response.data;
          }
        },
        function(response) {
          console.error(response);
        }
      );
    },
    registration() {
      if (
        this.demand.fullName != "" &&
        this.demand.phone != "" &&
        this.demand.needs != "" &&
        this.demand.time != ""
      ) {
        let demand = {
          fullName: this.demand.fullName,
          phone: this.demand.phone,
          needs: this.demand.needs,
          time: this.demand.time
        };
        this.$http.post(url.host + url.link_demand, demand).then(
          function(response) {
            if (response.status == 200) {
              this.demand = {
                fullName: "",
                phone: "",
                needs: "",
                time: ""
              };
              toastr.info("Đăng ký thành công", "Thành công!", {
                timeOut: 2000
              });
            } else {
              toastr.error("Đăng ký liệu thất bại", "Gặp lỗi!", {
                timeOut: 2000
              });
            }
          },
          function(response) {
            toastr.error("Đăng ký liệu thất bại", "Gặp lỗi!", {
              timeOut: 2000
            });
            console.error(response);
          }
        );
      } else {
        toastr.error("Bạn cần nhập đủ thông tin", "Gặp lỗi!", {
          timeOut: 2000
        });
      }
    }
  }
});
