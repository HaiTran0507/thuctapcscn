Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#studentIndex",
  data() {
    return {
      access_token: "",
      userId: "",
      list_cours: [],
      img: "../../images/course2.jpg"
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    this.userId = window.localStorage.getItem("userId");
    if (role != "ROLE_USER" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getListCours();
  },
  methods: {
    getListCours() {
      this.$http
        .get(url.host + url.link_course + `/my-course?userId= ${this.userId}`, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_cours = response.data;
              console.log(this.list_cours);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    showPopup(value) {
      // bootbox.alert("test");
      // this.popup = value;
      console.log(value);
      var modal_content = document.getElementById("showContent");
      console.log(modal_content);
      bootbox
        .dialog({
          message: modal_content,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: true,
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          modal_content.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          modal_content.style.display = "none";
          document.body.appendChild(modal_content);
        })
        .modal("show");
    }
  }
});
