Vue.component("menu-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/menu.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#danhsachQC",
  data() {
    return {
      list_adv: [],
      adv: { title: "", body: "", urlImage: "", status: true },
      form_title: "",
      text_submit: "",
      is_create: false,
      is_update: false,
      access_token: "",
      search: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getAdv();
  },
  methods: {
    getAdv() {
      this.$http
        .get(url.host + url.link_adv + `/find?title=${this.search}`, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              //    console.log(response)
              this.list_adv = response.data;
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    createAVD() {
      this.form_title = "Thêm quảng cáo";
      this.is_create = true;
      this.is_update = false;
      this.adv = { title: "", body: "", urlImage: "", status: true };
      this.text_submit = "Tạo mới";
      this.showBootBox();
    },
    updateAVD(item) {
      console.log("danh");
      this.form_title = "Sửa quảng cáo";
      this.is_create = false;
      this.is_update = true;
      this.adv = item;
      this.text_submit = "Thay đổi";
      this.showBootBox();
    },
    showBootBox() {
      var formADV = document.getElementById("formADV");
      bootbox
        .dialog({
          message: formADV,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: false,
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          formADV.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          formADV.style.display = "none";
          document.body.appendChild(formADV);
        })
        .modal("show");
    },
    handleSubmit() {
      if (this.is_create == true && this.is_update == false) {
        if (
          this.adv.title != "" &&
          this.adv.urlImage != "" &&
          this.adv.body != ""
        ) {
          this.$http
            .post(
              url.host + url.link_adv,
              {
                title: this.adv.title,
                body: this.adv.body,
                url_image: this.adv.urlImage,
                status: this.adv.status
              },
              {
                headers: {
                  Authorization: this.access_token
                }
              }
            )
            .then(
              function(response) {
                if (response.status == 200) {
                  console.log(response);
                  this.getAdv();
                  toastr.info("Thêm dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                } else {
                  toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function(response) {
                console.error(response);
              }
            );
        } else {
          console.log(2);
          toastr.error("Bạn điền thiếu thông tin", "Gặp lỗi!", {
            timeOut: 2000
          });
        }
      } else if (this.is_create == false && this.is_update == true) {
        if (
          this.adv.title != "" &&
          this.adv.urlImage != "" &&
          this.adv.body != ""
        ) {
          this.$http
            .put(
              url.host + url.link_adv,
              {
                id: this.adv.id,
                title: this.adv.title,
                body: this.adv.body,
                urlImage: this.adv.urlImage,
                status: this.adv.status
              },
              {
                headers: {
                  Authorization: this.access_token
                }
              },
              console.log(this.adv)
            )
            .then(
              function(response) {
                if (response.status == 200) {
                  console.log(response);
                  this.getAdv();
                  toastr.info("Sửa dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                } else {
                  toastr.info("Sửa dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function(response) {
                console.error(response);
              }
            );
        }
      }
    },
    deleteAVD(id) {
      var vm = this;
      bootbox.confirm({
        message: "Bạn muốn xóa quảng cáo?",
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success"
          },
          cancel: {
            label: "No",
            className: "btn-danger"
          }
        },
        callback: function(result) {
          if (result == true) {
            vm.$http
              .delete(url.host + url.link_adv + `?id=${id}`, {
                headers: {
                  Authorization: vm.access_token
                }
              })
              .then(
                function(response) {
                  if (response.status == 200) {
                    this.getAdv();
                    toastr.info("Xóa quảng cáo thành công", "Thành công!", {
                      timeOut: 2000
                    });
                  } else {
                    toastr.error("Xóa quảng cáo thất bại", "Gặp lỗi!", {
                      timeOut: 2000
                    });
                  }
                },
                function(response) {
                  toastr.error("Xóa quảng cáo thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                  console.error(response);
                }
              );
          }
        }
      });
    },
    seachADV() {
      this.getAdv();
    }
  }
});
