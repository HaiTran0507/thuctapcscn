Vue.component("menu-component", function(resolve, reject) {
  jQuery.ajax({ url: "../../../component/menu.html" }).done(function(template) {
    resolve({
      template: template
    });
  });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#adminIndex",
  data() {
    return {
      form_title: "",
      account: {
        role: "admin"
      },
      list_accounts: [],
      access_token: "",
      search: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getListAccount();
  },
  methods: {
    editAccount(item) {
      var formAccount = document.getElementById("formAccountEdit");
      this.form_title = "Sửa tài khoản";
      this.account = item;
      bootbox
        .dialog({
          message: formAccount,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: true,
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          formAccount.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          formAccount.style.display = "none";
          document.body.appendChild(formAccount);
        })
        .modal("show");
    },
    createAccount() {
      var formAccount = document.getElementById("formAccountCreate");
      this.form_title = "Thêm tài khoản";
      this.account = {
        full_name: "",
        role: "admin",
        sex: "nam",
        password: "",
        re_password: "",
        email: "",
        username: "",
        phone: "",
        price_each_lession: ""
      };
      bootbox
        .dialog({
          message: formAccount,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: false,
          className: "fullscreen",
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          formAccount.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          formAccount.style.display = "none";
          document.body.appendChild(formAccount);
        })
        .modal("show");
    },
    getListAccount() {
      // console.log(this.access_token);
      this.$http
        .get(url.host + url.link_user, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_accounts = response.data;
              // console.log(this.list_accounts);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    handleSubmitEdit() {
      if (this.account.full_name != "" && this.account.phone != "") {
        let account = {
          id: this.account.id,
          full_name: this.account.full_name,
          phone: this.account.phone,
          sex: this.account.sex,
          password: this.account.password,
          type_account: this.account.type_account
        };
        if (this.account.type_account == "teacher")
          account.price_each_lession = this.account.price_each_lession;
        this.$http
          .put(url.host + url.link_user, account, {
            headers: {
              Authorization: this.access_token
            }
          })
          .then(
            function(response) {
              if (response.status == 200) {
                this.getListAccount();
                toastr.info("Sửa dữ liệu thành công", "Thành công!", {
                  timeOut: 2000
                });
              } else {
                toastr.error("Sửa dữ liệu thất bại", "Gặp lỗi!", {
                  timeOut: 2000
                });
              }
            },
            function(response) {
              toastr.error("Sửa dữ liệu thất bại", "Gặp lỗi!", {
                timeOut: 2000
              });
              console.error(response);
            }
          );
      }
    },
    handleSubmitCreate() {
      let account = {
        full_name: this.account.full_name,
        username: this.account.username,
        email: this.account.email,
        password: this.account.password,
        re_password: this.account.re_password,
        phone: this.account.phone,
        role: this.account.role,
        sex: this.account.sex,
        price_each_lesson: this.account.price_each_lession,
        type_account: this.account.role
      };
      if (
        account.full_name != "" &&
        account.username != "" &&
        account.email != "" &&
        account.password != "" &&
        account.re_password != "" &&
        account.password == account.re_password &&
        account.phone != ""
      ) {
        if (account.role != "teacher") {
          delete account.price_each_lesson;
        }
        delete account.re_password;
        // console.log(account);
        this.$http.post(url.host + url.create_account, account).then(
          function(response) {
            if (response.status == 200) {
              this.getListAccount();
              toastr.info("Thêm dữ liệu thành công", "Thành công!", {
                timeOut: 2000
              });
            } else {
              toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                timeOut: 2000
              });
            }
          },
          function(response) {
            toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
              timeOut: 2000
            });
            console.error(response);
          }
        );
      } else {
        toastr.warning("Thông tin bạn nhập chưa đúng", "Gặp lỗi!", {
          timeOut: 2000
        });
      }
    },
    deleteAccount(account) {
      var vm = this;
      bootbox.confirm({
        message: "Bạn muốn xóa tài khoản: " + account.full_name,
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success"
          },
          cancel: {
            label: "No",
            className: "btn-danger"
          }
        },
        callback: function(result) {
          if (result == true) {
            vm.$http
              .delete(url.host + url.link_user + `?id=` + account.id, {
                headers: {
                  Authorization: vm.access_token
                }
              })
              .then(
                function(response) {
                  if (response.status == 200) {
                    this.getListAccount();
                    toastr.info("Xóa tài khoản thành công", "Thành công!", {
                      timeOut: 2000
                    });
                  } else {
                    toastr.error("Xóa tài khoản thất bại", "Gặp lỗi!", {
                      timeOut: 2000
                    });
                  }
                },
                function(response) {
                  toastr.error("Xóa tài khoản thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                  console.error(response);
                }
              );
          }
        }
      });
    },
    seachAccount() {
      this.$http
        .get(url.host + url.link_user + `/find?name=${this.search}`, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_accounts = response.data;
              // console.log(this.list_accounts);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    }
  },
  filters: {
    date(value) {
      var date = new Date(value);
      return (
        date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
      );
    },
    money(value) {
      var x = value;
      x = x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
      return x + " VNĐ";
    }
  }
});
