Vue.component("menu-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/menu.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#danhsachKH",
  data() {
    return {
      list_course: [],
      list_teacher: [],
      list_schedule: [],
      course: {
        name: "",
        startDate: "",
        endDate: "",
        numberSession: "",
        priceCourse: "",
        teacherCourse: "",
        learnSchedules: []
      },
      form_title: "",
      text_submit: "",
      is_create: false,
      is_update: false,
      access_token: "",
      search: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getListCourse();
    this.getListTeacher();
    this.getListSchedule();
  },
  methods: {
    getListCourse() {
      this.$http
        .get(url.host + url.link_course, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_course = response.data;
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    getListTeacher() {
      this.$http
        .get(url.host + url.link_user + "/teacher", {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_teacher = response.data;
              console.log(this.list_teacher);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    getListSchedule() {
      this.$http
        .get(url.host + url.link_schedule, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_schedule = response.data;
              // console.log(this.list_schedule);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    createCourse() {
      this.form_title = "Thêm khóa học";
      this.is_create = true;
      this.is_update = false;
      this.course = {
        name: "",
        startDate: "",
        endDate: "",
        numberSession: "",
        priceCourse: "",
        teacherCourse: "",
        learnSchedules: [],
        documentDriver: ""
      };
      this.text_submit = "Tạo mới";
      this.showBootBox();
    },
    updateCourse(item) {
      this.form_title = "Sửa khóa học";
      this.is_create = false;
      this.is_update = true;
      this.course = {
        id: item.id,
        startDate: this.conert24Hours(item.start_date),
        endDate: this.conert24Hours(item.end_date),
        numberSession: item.number_session,
        priceCourse: item.price_course,
        teacherCourse: item.teacher_id,
        status: item.status,
        name: item.name,
        // learnSchedules: item.learn_schedule,
        documentDriver: item.document
      };
      this.course.learnSchedules = [];
      for (let i = 0; i < item.learn_schedule.length; i++) {
        this.course.learnSchedules.push(item.learn_schedule[i].day_of_week);
      }
      this.text_submit = "Thay đổi";
      this.showBootBox();
    },
    showBootBox() {
      if (this.is_create == true)
        var formCourse = document.getElementById("formCreateCourse");
      // else var formCourse = document.getElementById("formEditCourse");
      else var formCourse = document.getElementById("formCreateCourse");
      bootbox
        .dialog({
          message: formCourse,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: false,
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function() {
          formCourse.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          formCourse.style.display = "none";
          document.body.appendChild(formCourse);
        })
        .modal("show");
    },
    handleSubmit() {
      if (
        this.course.name != "" &&
        this.course.startDate != "" &&
        this.course.endDate != "" &&
        this.course.numberSession != "" &&
        this.course.priceCourse != "" &&
        this.course.status != "" &&
        this.course.teacherCourse != "" &&
        this.course.documentDriver != "" &&
        this.course.learnSchedules.length != 0
      ) {
        let course = {
          name: this.course.name,
          startDate: this.course.startDate,
          endDate: this.course.endDate,
          numberSession: this.course.numberSession,
          priceCourse: this.course.priceCourse,
          status: 2,
          teacherCourse: {
            id: this.course.teacherCourse
          },
          documentDriver: this.course.documentDriver
        };
        course.learnSchedules = [];
        // console.log(this.course.length);
        for (let i = 0; i < this.course.learnSchedules.length; i++) {
          course.learnSchedules.push({ id: this.course.learnSchedules[i] });
        }
        if (this.is_create == true && this.is_update == false) {
          this.$http
            .post(url.host + url.link_course, course, {
              headers: {
                Authorization: this.access_token
              }
            })
            .then(
              function(response) {
                if (response.status == 200) {
                  toastr.info("Thêm dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                } else {
                  toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function(response) {
                console.error(response);
              }
            );
        } else {
          course.id = this.course.id;
          this.$http
            .put(url.host + url.link_course, course, {
              headers: {
                Authorization: this.access_token
              }
            })
            .then(
              function(response) {
                if (response.status == 200) {
                  toastr.info("Sửa dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                } else {
                  toastr.error("Sửa dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function(response) {
                console.error(response);
              }
            );
        }
      } else {
        toastr.error("Thông tin chưa đủ", "Chú ý", {
          timeOut: 2000
        });
      }
    },
    deleteCourse(item) {
      var vm = this;
      bootbox.confirm({
        message: "Bạn muốn khóa học: " + item.name,
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success"
          },
          cancel: {
            label: "No",
            className: "btn-danger"
          }
        },
        callback: function(result) {
          if (result == true) {
            vm.$http
              .delete(url.host + url.link_course + `?id=` + item.id, {
                headers: {
                  Authorization: vm.access_token
                }
              })
              .then(
                function(response) {
                  if (response.status == 200) {
                    this.getListCourse();
                    toastr.info("Xóa khóa học thành công", "Thành công!", {
                      timeOut: 2000
                    });
                  } else {
                    toastr.error("Xóa khóa học thất bại", "Gặp lỗi!", {
                      timeOut: 2000
                    });
                  }
                },
                function(response) {
                  toastr.error("Xóa khóa học thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                  console.error(response);
                }
              );
          }
        }
      });
    },
    conert24Hours(value) {
      value = value.split(":");
      return value[0] + ":" + value[1];
    },
    seachCourse() {
      if (this.search != "") {
        this.$http
          .get(url.host + url.link_course + `/find?name=${this.search}`, {
            headers: {
              Authorization: this.access_token
            }
          })
          .then(
            function(response) {
              if (response.status == 200) {
                this.list_course = response.data;
              }
            },
            function(response) {
              console.error(response);
            }
          );
      } else {
        this.getListCourse();
      }
    }
  },
  filters: {
    date(value) {
      var date = new Date(value);
      return (
        date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
      );
    },
    money(value) {
      var x = value;
      x = x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
      return x + " VNĐ";
    },
    time(value) {
      value = value.split(":");
      return value[0] + ":" + value[1];
    }
  }
});
