Vue.component("menu-component", function(resolve, reject) {
    jQuery.ajax({ url: "../../../component/menu.html" }).done(function(template) {
      resolve({
        template: template
      });
    });
  });
  Vue.component("header-component", function(resolve, reject) {
    jQuery
      .ajax({ url: "../../../component/header.html" })
      .done(function(template) {
        resolve({
          template: template
        });
      });
  });
  import * as path from "../../../url.js";
  var url = path.url;
  new Vue({
    // router,
    el: "#thongkeHV",
    data() {
      return {
        is_show: false
      };
    },
    mounted() {
      var access_token = window.localStorage.getItem("access_token");
      var role = window.localStorage.getItem("ROLE");
      if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
        window.location.assign("/");
      }
    },
    methods: {
      showTable() {
        this.is_show = true;
      }
    }
  });
  