Vue.component("menu-component", function(resolve, reject) {
  jQuery.ajax({ url: "../../../component/menu.html" }).done(function(template) {
    resolve({
      template: template
    });
  });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#thongkeHP",
  data() {
    return {
      is_show: false,
      end_time: "",
      start_time: "",
      access_token: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
  },
  methods: {
    showTable() {
      this.is_show = true;
    },
    async showReport() {
      console.log(this.start_time, this.end_time);
      if (this.start_time != "" && this.end_time != "") {
        let start_time = this.date(this.start_time);
        let end_time = this.date(this.end_time);
        var buyerData = {
          labels: [],
          datasets: [
            {
              fillColor: "#cae1fb",
              strokeColor: "#ACC26D",
              pointColor: "#fff",
              pointStrokeColor: "#9DB86D",
              borderColor: "red",
              data: []
            }
          ]
        };
        await this.$http
          .get(
            url.host +
              url.link_report_money +
              `?start=${start_time}&end=${end_time}`,
            {
              headers: {
                Authorization: this.access_token
              }
            }
          )
          .then(
            function(response) {
              if (response.status == 200) {
                let data = response.data.data;
                console.log(data);
                for (let item of data) {
                  console.log(item);
                  buyerData.labels.push(item.month);
                  buyerData.datasets[0].data.push(item.total_revenue);
                }
              }
            },
            function(response) {
              console.error(response);
            }
          );
        console.log(buyerData);
        console.log(buyerData.labels, buyerData.datasets[0].data);
        var buyers = document.getElementById("buyers").getContext("2d");
        await new Chart(buyers).Line(buyerData, {
          showTooltips: false,
          onAnimationComplete: function() {}
        });
      }
    },
    date(value) {
      var date = new Date(value);
      return `${date
        .getFullYear()
        .toString()
        .padStart(4, "0")}-${(date.getMonth() + 1)
        .toString()
        .padStart(2, "0")}-${date
        .getDate()
        .toString()
        .padStart(2, "0")}`;
    }
  }
});
