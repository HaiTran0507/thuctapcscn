Vue.component("menu-component", function(resolve, reject) {
  jQuery.ajax({ url: "../../../component/menu.html" }).done(function(template) {
    resolve({
      template: template
    });
  });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#nhuCauHoc",
  data() {
    return {
      is_show: false,
      status: "",
      list_demand: [],
      access_token: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
  },
  methods: {
    showTable() {
      this.is_show = true;
      let str = "/status/find";
      if (this.status != "") {
        str += `?status=${this.status}`;
      }
      this.$http
        .get(url.host + url.link_demand + str, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_demand = response.data;
            } else {
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    updateDemand(item) {
      let demand = {
        id: item.id,
        fullName: item.fullName,
        phone: item.phone,
        needs: item.needs,
        time: item.time,
        status: 1
      };
      this.$http
        .put(url.host + url.link_demand, demand, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.showTable();
              toastr.info("Xác nhận thành công", "Thành công!", {
                timeOut: 2000
              });
            } else {
              toastr.warning("Lỗi xác nhận", "Lỗi!", {
                timeOut: 2000
              });
            }
          },
          function(response) {
            toastr.warning("Lỗi xác nhận", "Lỗi!", {
              timeOut: 2000
            });
            console.error(response);
          }
        );
    }
  },
  filters: {
    date(value) {
      var date = new Date(value);
      return (
        date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
      );
    }
  }
});
