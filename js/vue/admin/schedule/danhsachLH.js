Vue.component("menu-component", function (resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/menu.html" })
    .done(function (template) {
      resolve({
        template: template
      });
    });
});
Vue.component("header-component", function (resolve, reject) {
  jQuery
    .ajax({ url: "../../../../component/header.html" })
    .done(function (template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#danhsachLH",
  data() {
    return {
      list_schedules: [],
      schedule: {
        id:"",
        dayOfWeek: 2,
        startLearn: "",
        endLearn: ""
      },
      form_title: "",
      is_create: false,
      is_update: false,
      text_submit: "",
      access_token: "",
      day_of_week: [
        { id: 2, value: "Thứ 2" },
        { id: 3, value: "Thứ 3" },
        { id: 4, value: "Thứ 4" },
        { id: 5, value: "Thứ 5" },
        { id: 6, value: "Thứ 6" },
        { id: 7, value: "Thứ 7" },
        { id: 8, value: "Thứ 8" }
      ]
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getSchedule();
  },
  methods: {
    createSchedule() {
      this.form_title = "Thêm lịch học";
      this.is_create = true;
      this.is_update = false;
      this.text_submit = "Tạo mới";
      this.showBootBox();
    },
    updateSchedule(id) {
      var formSchedule = document.getElementById("formSchedule");
      this.form_title = "Sửa lịch học";
      this.is_create = false;
      this.is_update = true;
      this.text_submit = "Thay đổi";
      this.showBootBox();
      this.id=id;
    },
    showBootBox() {
      var formSchedule = document.getElementById("formSchedule");
      bootbox
        .dialog({
          message: formSchedule,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: false,
          show: true // We will show it manually later
        })
        .on("shown.bs.modal", function () {
          formSchedule.style.display = "block";
        })
        .on("hide.bs.modal", function () {
          formSchedule.style.display = "none";
          document.body.appendChild(formSchedule);
        })
        .modal("show");
    },
    handleSubmit() {
      // console.log("submite");
      console.log(this.schedule);
      if (this.is_create == true && this.is_update == false) {
        if (
          this.schedule.dayOfWeek != "" &&
          this.schedule.startLearn != "" &&
          this.schedule.endLearn != ""
        ) {
          let schedule = {
            dayOfWeek: this.schedule.dayOfWeek,
            startLearn: this.conert24Hours(this.schedule.startLearn),
            endLearn: this.conert24Hours(this.schedule.endLearn)
          };
          this.$http
            .post(url.host + url.link_schedule, schedule, {
              headers: {
                Authorization: this.access_token
              }
            })
            .then(
              function (response) {
                if (response.status == 200) {
                  toastr.info("Thêm dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                  this.getSchedule();
                } else {
                  toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function (response) {
                console.error(response);
              }
            );
        } else {
          toastr.error("Thông tin chưa đủ", "Chú ý", {
            timeOut: 2000
          });
        }
      } else if (this.is_create == false && this.is_update == true) {
        if (
          this.schedule.dayOfWeek != "" &&
          this.schedule.startLearn != "" &&
          this.schedule.endLearn != ""
        ) {
          this.$http
            .put(
              url.host + url.link_schedule, {
                id: this.id,
                dayOfWeek:this.schedule.dayOfWeek,
                startLearn:this.schedule.startLearn,
                endLearn:this.schedule.endLearn
              },
              {
                headers: {
                  Authorization: this.access_token
                }
              }, console.log(this.adv)

            )
            .then(
              function (response) {
                if (response.status == 200) {
                  console.log(response);
                  toastr.info("Sửa dữ liệu thành công", "Thành công!", {
                    timeOut: 2000
                  });
                } else {
                  toastr.info("Sửa dữ liệu thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                }
              },
              function (response) {
                console.error(response);
              }
            );
        }
      }
    },
    //  updateSchedule() {
    //   if (this.schedule.dayOfWeek != "" &&
    //   this.schedule.startLearn != "" &&
    //   this.schedule.endLearn != "") {
    //     let schedule = {
    //       dayOfWeek: this.schedule.dayOfWeek,
    //       startLearn: this.conert24Hours(this.schedule.startLearn),
    //       endLearn: this.conert24Hours(this.schedule.endLearn)
    //     };
    //     this.$http
    //       .put(url.host + url.link_schedule, schedule, {
    //         headers: {
    //           Authorization: this.access_token
    //         }
    //       })
    //       .then(
    //         function (response) {
    //           if (response.status == 200) {
    //             this.getListAccount();
    //             toastr.info("Sửa dữ liệu thành công", "Thành công!", {
    //               timeOut: 2000
    //             });
    //           } else {
    //             toastr.error("Sửa dữ liệu thất bại", "Gặp lỗi!", {
    //               timeOut: 2000
    //             });
    //           }
    //         },
    //         function (response) {
    //           toastr.error("Sửa dữ liệu thất bại", "Gặp lỗi!", {
    //             timeOut: 2000
    //           });
    //           console.error(response);
    //         }
    //       );
    //   }
    // },
    deleteSchedule(id) {
      var vm = this;
      bootbox.confirm({
        message: "Bạn chắc chắn muốn xóa?",
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success"
          },
          cancel: {
            label: "No",
            className: "btn-danger"
          }
        },
        callback: function (result) {
          if (result == true) {
            vm.$http
              .delete(url.host + url.link_schedule + `?id=${id}`, {
                headers: {
                  Authorization: vm.access_token
                }
              })
              .then(
                function (response) {
                  if (response.status == 200) {

                    toastr.info("Xóa quảng cáo thành công", "Thành công!", {
                      timeOut: 2000
                    });
                  } else {
                    toastr.error("Xóa quảng cáo thất bại", "Gặp lỗi!", {
                      timeOut: 2000
                    });
                  }
                },
                function (response) {
                  toastr.error("Xóa quảng cáo thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                  console.error(response);
                }
              );
          }
        }
      });
    },
    getSchedule() {
      // this.$http.headers.common["Authorization"] = this.access_token;
      this.$http
        .get(url.host + url.link_schedule, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function (response) {
            if (response.status == 200) {
              console.log(response);
              this.list_schedules = response.data;
            }
          },
          function (response) {
            console.error(response);
          }
        );
    }, conert24Hours(value) {
      let hh = "";
      let mm = "";
      let str = value.split(":");
      hh = str[0];
      mm = str[1].replace("pm");
      mm = mm.replace("PM");
      mm = mm.replace("AM");
      mm = mm.replace("am");
      if (value.includes("PM") || value.includes("pm")) {
        hh = Number(hh) + 12;
      }
      return hh + ":" + mm;
    }
  },
  filters: {
    time(value) {
      value = value.split(":");
      return value[0] + ":" + value[1];
    }
  }
});
