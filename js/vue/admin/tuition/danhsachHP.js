Vue.component("menu-component", function(resolve, reject) {
  jQuery.ajax({ url: "../../../component/menu.html" }).done(function(template) {
    resolve({
      template: template
    });
  });
});
Vue.component("header-component", function(resolve, reject) {
  jQuery
    .ajax({ url: "../../../component/header.html" })
    .done(function(template) {
      resolve({
        template: template
      });
    });
});
import * as path from "../../../url.js";
var url = path.url;
new Vue({
  // router,
  el: "#hocphiIndex",
  data() {
    return {
      is_show: false,
      status: [{ id: 0, value: "Chưa đóng" }, { id: 1, value: "Đã đóng" }],
      list_course: [],
      form_title: "",
      is_create: false,
      is_update: false,
      list_accounts: [],
      tuition: {
        moneyPaid: "",
        courseTuition: "",
        userTuition: ""
      },
      access_token: "",
      listTuition: [],
      idTeacher: ""
    };
  },
  mounted() {
    var access_token = window.localStorage.getItem("access_token");
    this.access_token = access_token;
    var role = window.localStorage.getItem("ROLE");
    if (role != "ROLE_ADMIN" || !role || !access_token || access_token == "") {
      window.location.assign("/");
    }
    this.getListCourse();
    this.getListAccount();
  },
  methods: {
    showTable() {
      this.is_show = true;
      let idTeacher = this.idTeacher;
      // console.log(url.host + /api/course/tuition/ + idTeacher);
      this.$http
        .get(url.host + url.link_course + "/tuition/" + idTeacher, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.listTuition = response.data;
              console.log(response.data);
            } else {
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    getListCourse() {
      this.$http
        .get(url.host + url.link_course, {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_course = response.data;
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    createHP() {
      var formHP = document.getElementById("formHP");
      this.form_title = "Thêm học phí";
      bootbox
        .dialog({
          message: formHP,
          show: true,
          backdrop: true,
          animate: true,
          size: "big",
          onEscape: false
        })
        .on("shown.bs.modal", function() {
          formHP.style.display = "block";
        })
        .on("hide.bs.modal", function() {
          formHP.style.display = "none";
          document.body.appendChild(formHP);
        })
        .modal("show");
    },
    getListAccount() {
      // console.log(this.access_token);
      this.$http
        .get(url.host + url.link_user + "/student", {
          headers: {
            Authorization: this.access_token
          }
        })
        .then(
          function(response) {
            if (response.status == 200) {
              this.list_accounts = response.data;
              console.log(this.list_accounts);
            }
          },
          function(response) {
            console.error(response);
          }
        );
    },
    handleSubmit() {
      if (
        this.tuition.moneyPaid != "" &&
        this.tuition.courseTuition != "" &&
        this.tuition.userTuition != ""
      ) {
        let tuition = {
          moneyPaid: this.tuition.moneyPaid,
          courseTuition: {
            id: this.tuition.courseTuition
          },
          userTuition: {
            id: this.tuition.userTuition
          }
        };
        this.$http
          .post(url.host + url.link_tuition, tuition, {
            headers: {
              Authorization: this.access_token
            }
          })
          .then(
            function(response) {
              if (response.status == 200) {
                // this.getListAccount();
                toastr.info("Thêm dữ liệu thành công", "Thành công!", {
                  timeOut: 2000
                });
              } else {
                toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                  timeOut: 2000
                });
              }
            },
            function(response) {
              toastr.error("Thêm dữ liệu thất bại", "Gặp lỗi!", {
                timeOut: 2000
              });
              console.error(response);
            }
          );
      } else {
        toastr.error("Thông tin đang bỏ trống", "Gặp lỗi!", {
          timeOut: 2000
        });
      }
    },
    deleteHP(item) {
      var vm = this;
      bootbox.confirm({
        message: "Bạn muốn xóa?",
        buttons: {
          confirm: {
            label: "Yes",
            className: "btn-success"
          },
          cancel: {
            label: "No",
            className: "btn-danger"
          }
        },
        callback: function(result) {
          if (result == true) {
            vm.$http
              .delete(url.host + url.link_tuition + `?id=${item.id}`, {
                headers: {
                  Authorization: vm.access_token
                }
              })
              .then(
                function(response) {
                  if (response.status == 200) {
                    this.showTable();
                    toastr.info("Xóa thành công", "Thành công!", {
                      timeOut: 2000
                    });
                  } else {
                    toastr.error("Xóa thất bại", "Gặp lỗi!", {
                      timeOut: 2000
                    });
                  }
                },
                function(response) {
                  toastr.error("Xóa thất bại", "Gặp lỗi!", {
                    timeOut: 2000
                  });
                  console.error(response);
                }
              );
          }
        }
      });
    }
  }
});
