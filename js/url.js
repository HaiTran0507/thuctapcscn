export const url = {
  host: "https://2bd567e7.ngrok.io",
  signin: "/api/auth/signin",
  get_adv: "/api/public-adv",
  link_adv: "/api/adv",
  link_schedule: "/api/learn-schedule",
  link_user: "/api/user",
  link_course: "/api/course",
  create_account: "/api/auth/signup",
  link_demand: "/api/demand",
  link_tuition: "/api/tuition",
  link_checkin: "/api/attendance/checkin",
  link_timekeeping: "/api/timekeeping",
  link_report: "/api/user/student/statistical",
  link_caculate: "/api/attendance/caculate",
  link_report_money: "/api/tuition/statistical"
};
